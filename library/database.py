#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import sqlite3


DB_NAME = '.meraki.db'

class Database():  
    def __init__(self):
        self.connection = sqlite3.connect(DB_NAME)

    def getDbName(self):
        return DB_NAME

    def create(self):
        self.connection.execute('CREATE TABLE IF NOT EXISTS access (macAddress text primary key, networkId text, serialNumber text)')
        self.connection.execute('CREATE TABLE IF NOT EXISTS client (macAddress text primary key, networkId text, serialNumber text)')
        self.connection.execute('CREATE TABLE IF NOT EXISTS switch (macAddress text primary key, networkId text, serialNumber text)')
        self.connection.execute('CREATE TABLE IF NOT EXISTS info (organization text primary key, access integer, client integer, switch integer, created text)')

    def insert(self, tableName, itemList):
        try:
            if tableName == 'info':
                self.connection.execute('INSERT INTO {} VALUES (?,?,?,?,?)'.format(tableName), itemList)
            else:
                self.connection.executemany('INSERT INTO {} VALUES (?,?,?)'.format(tableName), itemList)
        except Exception as e:
            print(f'Error: {e}')

    def select(self, macAddress):
        for table in self.connection.execute('SELECT name FROM sqlite_master WHERE type = "table"'):
            try:
                query = self.connection.execute('SELECT * FROM {} WHERE macAddress = ?'.format(table[0]), macAddress).fetchone()
            except Exception as e:
                print(f'Error: {e}')
            if query is not None:
                return table, query

    def selectInfo(self):
        return self.connection.execute('SELECT * FROM info')
      
    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.connection.commit()
        self.connection.close()