#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import re
import asyncio

import meraki.aio

from .output import PrettyPrint
from .database import Database
from .dashboard import listNetworkClient, listNetworkDevice, listSwitchPorts, listSwitchPortStatuses


async def macLookup(aiomeraki: meraki.aio.AsyncDashboardAPI):
    while True:
        macAddress = None
        try:
            while not macAddress:
                macAddress = (input(f'Enter valid mac address: '), )
                if not re.match("[0-9a-f]{2}([:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", macAddress[0]):   
                    macAddress = None  
            with Database() as db:
                result = db.select(macAddress)
            if result is not None:
                if result[0][0] == 'access':
                    PrettyPrint().access(await listNetworkDevice(aiomeraki, result[1][2]))
                if result[0][0] == 'client':
                    PrettyPrint().client(await listNetworkClient(aiomeraki, result[1][1], result[1][2]))
                elif result[0][0] == 'switch':
                    PrettyPrint().switch(await listNetworkClient(result[1][2]))
                    PrettyPrint().switchSettings(await listSwitchPorts(aiomeraki, result[1][2]), await listSwitchPortStatuses(aiomeraki, result[1][2]))
        except KeyboardInterrupt:
            print(f'\nExit')
            exit(0)