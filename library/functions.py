#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import os
import csv
import time
from datetime import datetime

from pytz import timezone

from .database import Database


DB_NAME = '.meraki.db'
PROXY_SETTINGS = 'proxy.txt'

def proxySettings():
    if os.path.exists(PROXY_SETTINGS):
        with open(PROXY_SETTINGS, 'r') as f:
            return f.readline()
    return None

def getAdminName(admins, adminId):
    for admin in admins:
        if admin['id'] == adminId:
            return admin['name']

def mergeApiRequest(path, queryString):
    query = 'https:/' + path
    if queryString:
        query += '?' + queryString
    return query

def getHttpMessage(code):
    if code == 200:
        return 'OK'
    elif code == 400:
        return 'Bad Request' 
        
def checkIfDbExists():
    if not os.path.exists(DB_NAME):
        return False
    if os.path.exists(DB_NAME):
        currentTime = time.time()
        modifiedDate = os.path.getmtime(DB_NAME)
        if currentTime - modifiedDate > 86400:
            answer = input(f'Database older than 24 hours! Reinitiate? (y, N) ')
            if answer == 'y':
                os.remove(DB_NAME)
                return False
    return True

def convertTimestamp(timestamp):
    # convert string to datetime object
    timestamp = datetime.strptime(timestamp, '%Y-%m-%dT%H:%M:%SZ')
    # specify current timestamp timezone (utc)
    timestamp = timezone('UTC').localize(timestamp)
    # specify new timezone and return formated timestamp
    return timezone('Europe/Berlin').normalize(timestamp).strftime('%d.%m.%y %H:%M:%S')

def writeListToFile(fileName, fieldNames, itemList):
    with open(fileName, mode='w', newline='\n') as f:
        writer = csv.DictWriter(f, fieldnames = fieldNames, delimiter = ',')
        writer.writeheader()
        writer.writerows(itemList)

def writeListToDatabase(*values):
    with Database() as db:
        db.create()
        for tableName, itemList in zip(*values):
            db.insert(tableName, itemList)