#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import asyncio

import meraki.aio

from .output import PrettyPrint
from .dashboard import listOrganizations, listOrganizationAdmins, listApiRequests
from .functions import getAdminName, mergeApiRequest, getHttpMessage, convertTimestamp, writeListToFile

async def apiRequest(aiomeraki: meraki.aio.AsyncDashboardAPI):
    apiRequestsList = list()
    organizations = await listOrganizations(aiomeraki)
    PrettyPrint().organization(organizations)
    organizationNumber = input(f'Enter number: ')
    timespan = input(f'Enter timespan: ')
    try:
        admins = await listOrganizationAdmins(aiomeraki, organizations[int(organizationNumber)-1]['id'])
        requests = await listApiRequests(aiomeraki, organizations[int(organizationNumber)-1]['id'], timespan)
    except meraki.AsyncAPIError as e:
        print(f'Error: {e}')
        exit(1)
    for request in requests:
        apiRequestsList.append(
            {
                'ID' : request['adminId'], 
                'Name' : getAdminName(admins, request['adminId']), 
                'Method' : request['method'],
                'Request' : mergeApiRequest(request['path'], request['queryString']), 
                'Status' : getHttpMessage(request['responseCode']), 
                'Timestamp' : convertTimestamp(request['ts'])

            }
        )
    fileName = f'ApiRequests_from_{organizations[int(organizationNumber)-1]["id"]}.csv'
    fieldNames = ['ID', 'Name', 'Method', 'Request', 'Status', 'Timestamp']
    writeListToFile(fileName, fieldNames, apiRequestsList)
    print(f'File saved as {fileName}')