#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import asyncio
from datetime import datetime

import meraki.aio

from .output import PrettyPrint
from .functions import writeListToDatabase

   
async def initiateDb(aiomeraki: meraki.aio.AsyncDashboardAPI):
    accessList = list()
    clientList = list()
    switchList = list()
    organizations = await listOrganizations(aiomeraki)
    print(f'Organizations found: ')
    PrettyPrint().organization(organizations)
    organizationNumber = input(f'Enter organization number: ')
    networks = await listNetworks(aiomeraki, organizations[int(organizationNumber)-1]['id'])
    deviceTasks = [listNetworkDevices(aiomeraki, network['id']) for network in networks]
    for task in asyncio.as_completed(deviceTasks):
        try:
            devices = await task
            for device in devices:
                if device['firmware'].split('-')[0] == 'wireless':
                    accessList.append((device['mac'], device['networkId'], device['serial']))
                if device['firmware'].split('-')[0] == 'switch':
                    switchList.append((device['mac'], device['networkId'], device['serial']))
        except Exception:
            continue
    clientTasks = [listNetworkClients(aiomeraki, network['id']) for network in networks]
    for task in asyncio.as_completed(clientTasks):
        try:
            clients = await task
            for client in clients[0]:
                clientList.append((client['mac'], clients[1], client['id']))
        except Exception:
            continue
    infoList = [organizations[int(organizationNumber)-1]['name'], len(accessList), len(clientList), len(switchList), datetime.now().strftime('%d.%m.%y %H:%M:%S')]
    tableName = ('info', 'access', 'client', 'switch')
    itemLists = (infoList, accessList, clientList, switchList)
    writeListToDatabase(tableName, itemLists)

async def listOrganizations(aiomeraki: meraki.aio.AsyncDashboardAPI):
    return await aiomeraki.organizations.getOrganizations()

async def listNetworks(aiomeraki: meraki.aio.AsyncDashboardAPI, organizationId):
    return await aiomeraki.organizations.getOrganizationNetworks(organizationId)

async def listNetworkDevice(aiomeraki: meraki.aio.AsyncDashboardAPI, serial):
    return await aiomeraki.devices.getDevice(serial)

async def listNetworkDevices(aiomeraki: meraki.aio.AsyncDashboardAPI, networkId):
    return await aiomeraki.networks.getNetworkDevices(networkId)

async def listNetworkClient(aiomeraki: meraki.aio.AsyncDashboardAPI, networkId, clientId):
    return await aiomeraki.networks.getNetworkClient(networkId, clientId)

async def listNetworkClients(aiomeraki: meraki.aio.AsyncDashboardAPI, networkId):
    return (await aiomeraki.networks.getNetworkClients(networkId, timespan=60*60*24*14, perPage=1000, total_pages='all'), networkId)

async def listSwitchPorts(aiomeraki: meraki.aio.AsyncDashboardAPI, serial):
    return await aiomeraki.switch.getDeviceSwitchPort(serial)

async def listSwitchPortStatuses(aiomeraki: meraki.aio.AsyncDashboardAPI, serial):
    return await aiomeraki.switch.getDeviceSwitchPortsStatuses(serial)

async def listOrganizationAdmins(aiomeraki: meraki.aio.AsyncDashboardAPI, organizationId):
    return await aiomeraki.organizations.getOrganizationAdmins(organizationId)

async def listApiRequests(aiomeraki: meraki.aio.AsyncDashboardAPI, organizationId, timespan):
    return await aiomeraki.organizations.getOrganizationApiRequests(organizationId, total_pages='all', timespan=timespan)