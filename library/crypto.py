#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import os
import base64
import getpass

import cryptography.exceptions
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


SALT = '.salt'
FILE_NAME = '.secret'

class Crypto(): 
    def __init__(self, passphrase):
        self.kdf = PBKDF2HMAC(
            algorithm=hashes.SHA512_256,
            length=32,
            salt=self.getSaltVal(),
            iterations=100000,
            backend=default_backend()    
        )
        self.passphrase = passphrase
        self.fernet = Fernet(base64.urlsafe_b64encode(self.kdf.derive(self.passphrase)))
        if not os.path.exists(FILE_NAME):
            self.encrypt(getpass.getpass(prompt=f'Enter your personal API key: ').encode())

    def getSaltVal(self):
        if os.path.exists(SALT):
            with open(SALT, 'rb') as f:
                return f.read()      
        if not os.path.exists(SALT):
            saltVal = os.urandom(16)
            with open(SALT, 'wb') as f:
                f.write(saltVal)
                return saltVal

    def encrypt(self, merakiKey):
        encrypted = self.fernet.encrypt(merakiKey)
        with open(FILE_NAME, 'wb') as f:
            f.write(encrypted)

    def decrypt(self):
        with open(FILE_NAME, 'rb') as f:
            data = f.read()
        try:
            return self.fernet.decrypt(data).decode()
        except cryptography.fernet.InvalidToken:
            return None