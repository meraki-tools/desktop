#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

from prettytable import PrettyTable

from .database import Database


class PrettyPrint():
    def __init__(self):
        self.table = PrettyTable()

    def organization(self, organizations):
        self.table.field_names = ['', 'Name', 'ID']
        for number, organization in enumerate(organizations, start=1):
            self.table.add_row([number, organization['name'], organization['id']])
        print(self.table)

    def access(self, access):
        self.table.field_names = ['ID', 'Name', 'Tags', 'IP', 'MAC', 'Network ID', 'Model', 'Firmware']
        self.table.add_row([access['serial'], access['name'], access['tags'], access['lanIp'], access['mac'], access['networkId'], access['model'], access['firmware']])
        print(self.table)
    
    def client(self, client):
        self.table.field_names = ['ID', 'Name', 'IP', 'MAC', 'Status', 'Connected to']
        self.table.add_row([client['id'], client['description'], client['ip'], client['mac'], client['status'], client['recentDeviceMac']])
        print(self.table)

    def switch(self, switch):
        self.table.field_names = ['ID', 'Name', 'IP', 'MAC', 'Network ID', 'Model', 'Firmware']
        self.table.add_row([switch['serial'], switch['name'], switch['lanIp'], switch['mac'], switch['networkId'], switch['model'], switch['firmware']])
        print(self.table)

    def switchSettings(self, port, setting):
        self.table.field_names = ['Port', 'Name', 'Tags', 'Enabled', 'Status', 'PoE enabled', 'Type', 'VLAN', 'Voice VLAN', 'Allowed VLAN', 'Speed', 'Duplex']   
        for port, setting in zip(port, setting):
            self.table.add_row([port['portId'], port['name'], port['tags'], port['enabled'], setting['status'], port['poeEnabled'], port['type'], port['vlan'], port['voiceVlan'], port['allowedVlans'], setting['speed'], setting['duplex']])
        print(self.table) 

    def info(self):
        self.table.field_names = ['Organization', 'Access Points', 'Clients', 'Switches', 'Created']
        with Database() as db:
            for values in db.selectInfo():
                self.table.add_row(values)
        print(self.table)

    def scripts(self, scripts, descriptions):
        self.table.field_names = ['', 'Name', 'Description']
        for number, (script, description) in enumerate(zip(scripts, descriptions), start=1):
            self.table.add_row([number, script, description])
        print(self.table)