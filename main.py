#!/usr/bin/env python3

__author__  = 'Stefan'
__date__    = 'June 2020'
__license__ = "GNU GPLv3"
__version__ = '1.0'

import os
import getpass
import asyncio

import meraki.aio

from library.crypto import Crypto
from library.output import PrettyPrint
from library.dashboard import initiateDb
from library.macLookup import macLookup
from library.changePort import changePort
from library.apiRequest import apiRequest
from library.functions import checkIfDbExists, proxySettings


SCRIPTS = (
    'macLookup', 
    'apiRequest', 
    'changePort'
)
DESCRIPTION = (
    'Look up a MAC address and print information from Meraki API', 
    'Save Meraki API requests made by an organization as csv file',
    'Change port settings of a given switchport using Meraki API'
)
DB_NAME = '.meraki.db'

async def main(merakiKey=None):
    while not merakiKey:
        merakiKey = Crypto(getpass.getpass(prompt=f'Enter passphrase: ').encode()).decrypt()
    async with meraki.aio.AsyncDashboardAPI(
        api_key=merakiKey,
        output_log=False,
        print_console=False, 
        suppress_logging=True,
        requests_proxy=proxySettings()
    ) as aiomeraki:
        if not checkIfDbExists():
            await initiateDb(aiomeraki)
        os.system('clear')
        PrettyPrint().info()
        PrettyPrint().scripts(SCRIPTS, DESCRIPTION)
        answer = input(f'\nEnter script number: ')
        os.system('clear')
        if answer == '0':
            os.remove(DB_NAME)
            await initiateDb(aiomeraki)
        if answer == '1':
            await macLookup(aiomeraki)
        if answer == '2':
            await apiRequest(aiomeraki)
        if answer == '3':
            await changePort(aiomeraki)


if __name__ == '__main__':
    asyncio.run(main()) 